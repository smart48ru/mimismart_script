#!/bin/bash
#Smart48.ru Telegram bot v2.0
#Данный скрип сделан для отправки уведомлений в телеграм бот @Smart48_bot  на Cuarm3, Cuarm5M
#Для работы вам нужно подписаться на бота @Smart48_bot и узнать Ваш id
#
#Добавлено:
#1.Автоматическая установка всех компонентов
#2.Возможность отправки в бот скриншота с камер внедренных в систему Mimismart (протестирован rtsp поток)
#3.Возможность записывания видио продолжительностью до 60 секунд (больше не тестировали) и отправки его

#Установка
#Автоматическая 
#Скопируйте всю строку без # и запустите ее на вашем сервере статистики
#Скрипт определит какой у Вас сервер статистики и установит все компаненты.
#if [ "`cat /proc/cpuinfo | grep -c Omega`" -ne "0" ] ;then wget -O /tmp/install.sh http://tg.smart48.ru/distr/install.sh && ash /tmp/install.sh omega ; else wget -O /tmp/install.sh http://tg.smart48.ru/distr/install.sh && bash /tmp/install.sh raspberry;fi && rm /tmp/install.sh
#везде где надо нажать "Y" для установки нужных компонентов, если компоненты уже установлены на экран выведется 
#
#				!!ВНИМАНИЕ!! БЕЗ УСТАНОВКИ ВСЕХ КОМПОНЕНТОВ БОТ РАБОТАТЬ НЕ БУДЕТ!
#
#Истользование
#scripts/notifications/telegram.txt - пример работы с ботом.
#Для вызова бота в коде 
#setStatus(1000:90, "telegram_id||Текст сообщения||Id:subid камеры||время записи в секундах"); 

#Если нужно отправить нескольким клиента одно и тоже то telegram_id нужно указать через запятую
#setStatus(1000:90, "telegram_id,telegram_id2,telegram_id3,telegram_idn||Текст сообщения||Id:subid камеры||время записи в секундах"); 

#Пример: на id  12345678 получите сообщение "Тестовое сообщение" с прикрепленным видео с камеры 2048:1 продолжительностью 10 секунд  
#setStatus(1000:90, "12345678||Тестовое сообщение||2048:1||10");
#на id  96500921 получите сообщение "Тестовое сообщение" с прикрепленным фото с камеры 2048:1
#setStatus(1000:90, "12345678||Тестовое сообщение||2048:1||");
#на id  96500921 получите сообщение "Тестовое сообщение"
#setStatus(1000:90, "12345678||Тестовое сообщение||||");
#

case "$1" in
"raspberry")
mount -o remount,rw /
##проверка установки всех компонентов
		#проверка установки ffmpeg
if [ ! -e /usr/bin/ffmpeg ]
then
	echo "ffmpeg не установлен"
	wget http://tg.smart48.ru/distr/ffmpeg_3.1.1-1_armhf.deb
	dpkg -i ffmpeg_3.1.1-1_armhf.deb			
	rm ffmpeg_3.1.1-1_armhf.deb
fi
#Проверка установки curl
if [ ! -e /usr/bin/curl ]
then
	echo "curl не установлен"
	apt-get update
	apt-get install curl			
fi

#Проверка установки perl
if [ ! -e /usr/bin/perl ]
then
	echo "perl не установлен"
	apt-get update
	apt-get install perl
fi
wget -O /home/sh2/exe/90.sh tg.smart48.ru/distr/raspberry.sh
chmod +x /home/sh2/exe/90.sh 
wget -O /home/sh2/scripts/notifications/telegram.txt tg.smart48.ru/distr/telegram.txt
echo "scripts/notifications/telegram.txt - пример работы с ботом."
mount -o remount,ro /
;;


"omega")
mount -o remount,rw /
wget -O /mimismart/exe/90.sh tg.smart48.ru/distr/omega.sh
chmod +x /mimismart/exe/90.sh
wget -O /mimismart/scripts/notifications/telegram.txt tg.smart48.ru/distr/telegram_omega.txt

echo "scripts/notifications/telegram.txt - пример работы с ботом."
mount -o remount,ro /

;;


*)
  echo "start $0 omega|raspberry "
  echo "ash $0 omega - start instalation from Cuarm5, Micro (OMEGA)"
  echo "bash $0 raspberry - start instalation from Cuarm5m, Cuarm4, Cuarm3 (RASTBERRY)" 
esac
